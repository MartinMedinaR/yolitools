
create database if not exists tools;


create table if not exists `tools`.`clientes`(`id` int(3) not null auto_increment, `cliente` char(20) not null, `cedula` char(11) not null, `telefono` char(14) not null, `direccion` char(25) not null, `descuento` int(3) not null, `puntos` double not null, `pendiente_credito` double not null, `pendiente_otros` double not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;



create table if not exists `tools`.`facturas`(`factura` int(10) not null auto_increment, `fecha` char(20) not null, `hora` char(11) not null, `cliente` char(40) not null, `pago` double not null, `vueltos` double not null, `total` double not null, `estado` char(10) not null, `pendiente` double not null, PRIMARY KEY (`factura`)) ENGINE=InnoDB;



create table if not exists `tools`.`login`(`usuario` char(20) not null, `contrasena` char(30) not null, `permiso` char(30) not null, `nombres` char(40) not null) ENGINE=InnoDB;


INSERT INTO `tools`.`login`(`usuario`, `contrasena`, `permiso`, `nombres`) values ('admin', '123abc', 'ADMINISTRADOR', 'Martin Medina Ruvian'); 


create table if not exists `tools`.`productos`(`producto` char(37) not null, `cantidad` double not null, `presentacion` char(10) not null, `proveedor` char(40) not null, `precio_unidad` double not null, `codigo` char(15) not null, `costo_unidad` double not null, `descuento` int(90) not null, `final_descuento` date not null, `unidad` char(15) not null, `costo_cantidad` double not null) ENGINE=InnoDB;


create table if not exists `tools`.`proveedores`(`id` int(3) not null auto_increment, `proveedor` char(20) not null, `nit` char(13) not null, `contacto` char(20) not null, `telefono` char(11) not null, `telefono_empresa` char(11) not null, `direccion` char(25) not null, `pendiente_credito` double not null, `pendiente_otros` double not null, PRIMARY KEY(`id`)) ENGINE=InnoDB;
 
create table if not exists `tools`.`utilidad_dia`(`id` int(10) not null auto_increment, `producto` char(40) not null, `cantidad` double not null, `utilidad` double not null, `fecha` date not null, `hora` char(11) not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

ALTER TABLE `tools`.`utilidad_dia` ADD if not exists `cliente` char(32) not null after `hora`;


create table if not exists `tools`.`compras`(`id` int(9) not null auto_increment, `empresa` char(20) not null, `nit` char(13) not null, `num_factura` char(9) not null, `fecha` date not null, `excluido` double not null, `gravado` double not null, `iva` double not null, `total` double not null, `tipo_pago` char(8) not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

create table if not exists `tools`.`empleados`(`id` int(3) not null auto_increment, `empleado` char(20) not null, `identificacion` char(11) not null, `codigo` int(3) not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;


create table if not exists `tools`.`presentaciones`(`presentacion` char(25) not null) ENGINE=InnoDB;

create table if not exists `tools`.`registros_compras`(`id` int(9) not null auto_increment, `producto` char(37) not null, `costo_unidad` double not null, `cantidad` double not null, `costo_cantidad` double not null, `fecha` date not null, `hora` char(11) not null,  PRIMARY KEY (`id`)) ENGINE=InnoDB; 

ALTER TABLE `tools`.`registros_compras` DROP if exists `factura`;

ALTER TABLE `tools`.`registros_compras` DROP if exists `codigo`;

drop table if exists `tools`.`unidades`; 
create table if not exists `tools`.`unidades`(`id` int(3) not null auto_increment, `unidad` char(10) not null, PRIMARY KEY (`id`)) ENGINE=InnoDB; 
INSERT INTO `tools`.`unidades`(`unidad`) values ('UND');
INSERT INTO `tools`.`unidades`(`unidad`) values ('KILO');
INSERT INTO `tools`.`unidades`(`unidad`) values ('LIBRA');
INSERT INTO `tools`.`unidades`(`unidad`) values ('CAJA');
INSERT INTO `tools`.`unidades`(`unidad`) values ('METRO');


drop table if exists `tools`.`creditos`;
create table if not exists `tools`.`creditos`(`id` int(9) not null auto_increment, `proveedor` char(57) not null, `observacion` char(250) not null, `valor` double not null, `fecha` date not null, `fecha_limite` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;


create table if not exists `tools`.`infocaja`(`id` int(10) not null auto_increment, `fecha` date not null, `valor_base` double not null, `observacion_base` char(10) not null,`valor_cierre` double not null, `observacion_cierre` char(40) not null, `usuario` char(19) not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;



create table if not exists `tools`.`devoluciones`(`id` int(4) not null auto_increment, `fecha` date not null, `hora` char(11) not null, `producto` char(37) not null, `cantidad` double not null, `usuario` char(19) not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;


ALTER TABLE `tools`.`compras` ADD if not exists `usuario` char(20) not null after `tipo_pago`;

ALTER TABLE `tools`.`facturas` ADD if not exists `usuario` char(20) not null after `pendiente`;

ALTER TABLE `tools`.`facturas` ADD if not exists `usuario` char(20) not null after `pendiente`;





drop table if exists `tools`.`infoempresa`;

drop table if exists `tools`.`stock`;

ALTER TABLE `tools`.`compras` ADD if not exists `usuario` char(20) not null after `tipo_pago`;

ALTER TABLE `tools`.`facturas` ADD if not exists `usuario` char(20) not null after `pendiente`;

ALTER TABLE `tools`.`productos` ADD if not exists `cantidad_minima` DOUBLE NOT NULL AFTER `costo_cantidad`;

drop table if exists `tools`.`iva`;
create table if not exists `tools`.`iva`(`id` int(100) not null auto_increment, `observacion` char(100) not null, `porcentaje_iva` double not null, PRIMARY KEY (`id`)) ENGINE=InnoDB; 

INSERT INTO `tools`.`iva`(`observacion`,`porcentaje_iva`) values ('excluido','0');
INSERT INTO `tools`.`iva`(`observacion`,`porcentaje_iva`) values ('compra','5');
INSERT INTO `tools`.`iva`(`observacion`,`porcentaje_iva`) values ('venta','19');

ALTER TABLE `tools`.`registros_compras` DROP if exists `iva`;

ALTER TABLE `tools`.`productos` CHANGE `producto` `producto` char(47) not null;
ALTER TABLE `tools`.`productos` CHANGE `codigo` `codigo` char(13) not null;
ALTER TABLE `tools`.`productos` CHANGE `descuento` `descuento` int(3) not null;
alter table `tools`.`productos` change `unidad` `unidad` char(5) not null;
drop index if exists productos_codigo on `tools`.`productos`;
create index if not exists productos_name on `tools`.`productos`(producto);


ALTER TABLE `tools`.`facturas` DROP if exists `factura`;

ALTER TABLE `tools`.`facturas` ADD `factura` INT(9) NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`factura`);
alter table `tools`.`facturas` change `fecha` `fecha` date not null;
alter table `tools`.`facturas` change `cliente` `cliente` char(20) not null;
alter table `tools`.`facturas` change `estado` `estado` char(13) not null;
alter table `tools`.`facturas` change `usuario` `usuario` char(20) not null;
create index if not exists facturas_factura on `tools`.`facturas`(factura);
create index if not exists facturas_cliente on `tools`.`facturas`(cliente);
create index if not exists facturas_usuario on `tools`.`facturas`(usuario);


alter table `tools`.`login` change `usuario` `usuario` char(7) not null;
alter table `tools`.`login` change `contrasena` `contrasena` char(8) not null;
alter table `tools`.`login` change `nombres` `nombres` char(20) not null;

ALTER TABLE `tools`.`utilidad_dia` DROP if exists `id`;
ALTER TABLE `tools`.`utilidad_dia` ADD `id` INT(9) NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);
alter table `tools`.`utilidad_dia` change `producto` `producto` char(47) not null;

alter table `tools`.`compras` change `id` `id` int(9) not null auto_increment;
alter table `tools`.`compras` change `empresa` `empresa` char(25) not null;
alter table `tools`.`compras` change `num_factura` `num_factura` char(9) not null;
alter table `tools`.`compras` change `tipo_pago` `tipo_pago` char(8) not null;

alter table `tools`.`empleados` change `id` `id` int(3) not null auto_increment;
ALTER TABLE `tools`.`empleados` DROP if exists `tipo`;
ALTER TABLE `tools`.`empleados` DROP if exists `porcentaje`;
alter table `tools`.`empleados` change if exists `porcentaje` `sueldo` double not null;
ALTER TABLE `tools`.`empleados` ADD if not exists `sueldo` double not null after `codigo`;

alter table `tools`.`infocaja` change `id` `id` int(10) not null auto_increment;
alter table `tools`.`infocaja` change `observacion_base` `observacion_base` char(255) not null;
alter table `tools`.`infocaja` change `observacion_cierre` `observacion_cierre` char(255) not null;

alter table `tools`.`infocaja` change `usuario` `usuario` char(20) not null;

create table if not exists `tools`.`saldos`(`id` int(9) not null auto_increment, `cliente` char(32) not null, `observacion` char(250) not null, `valor` double not null, `fecha` date not null, `fecha_limite` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;


create table if not exists `tools`.`cuota_saldos`(`id` int(9) not null auto_increment,`cliente` char(32) not null,  `observacion` char(250) not null, `valor` double not null,`fecha` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;


create table if not exists `tools`.`egreso_empleado`(`id` int(9) not null auto_increment, `empleado` char(57) not null,`codigo` char(11) not null, `observacion` char(250) not null, `valor` double not null, `fecha` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

create table if not exists `tools`.`egreso_carcamo`(`id` int(9) not null auto_increment, `usuario` char(57) not null, `observacion` char(250) not null, `valor` double not null, `fecha` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

create table if not exists `tools`.`costo_ventas`(`id` int(10) not null auto_increment, `producto` char(255) not null, `cantidad` double not null, `costo` double not null, `fecha` date not null, `hora` char(11) not null, `cliente` char(32) not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

create table if not exists `tools`.`egreso_ganancias`(`id` int(9) not null auto_increment, `usuario` char(57) not null, `observacion` char(250) not null, `valor` double not null, `fecha` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

create table if not exists `tools`.`efectivo`(`id` int(9) not null auto_increment, `fecha_inicial` date not null,`fecha_final` date not null, `valor` double not null, `fecha` date not null, `observacion` char(250) not null,`usuario` char(20) not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

create table if not exists `tools`.`mis_cuotas`(`id` int(9) not null auto_increment,`proveedor` char(57) not null, `observacion` char(250) not null, `valor` double not null,`fecha` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

create table if not exists `tools`.`ingreso_creditos`(`id` int(9) not null auto_increment, `proveedor` char(57) not null, `observacion` char(250) not null, `valor` double not null, `fecha` date not null, `fecha_limite` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;


create table if not exists `tools`.`ingreso_saldos`(`id` int(9) not null auto_increment, `cliente` char(32) not null, `observacion` char(250) not null, `valor` double not null, `fecha` date not null, `fecha_limite` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

alter table `tools`.`clientes` change `cliente` `cliente` char(40) not null;
alter table `tools`.`clientes` change `direccion` `direccion` char(40) not null;
alter table `tools`.`clientes` change `cedula` `cedula` char(28) not null;
alter table `tools`.`ingreso_saldos` change `cliente` `cliente` char(32) not null;
alter table `tools`.`saldos` change `cliente` `cliente` char(32) not null;
alter table `tools`.`cuota_saldos` change `cliente` `cliente` char(32) not null;
alter table `tools`.`utilidad_dia` change `cliente` `cliente` char(32) not null;
alter table `tools`.`costo_ventas` change `cliente` `cliente` char(32) not null;
alter table `tools`.`facturas` change `cliente` `cliente` char(40) not null;
alter table `tools`.`proveedores` change `proveedor` `proveedor` char(40) not null;

alter table `tools`.`clientes` change if exists `pendiente_credito` `debe` double not null;

alter table `tools`.`clientes` DROP if exists `pendiente_otroS`;

create table if not exists `tools`.`deudas_terceros`(`id` int(9) not null auto_increment, `deudor` char(32) not null, `observacion` char(250) not null, `valor` double not null, `fecha` date not null, `fecha_limite` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

alter table `tools`.`proveedores` change if exists `pendiente_otros` `debe` double not null;

create table if not exists `tools`.`ingreso_efectivo`(`id` int(9) not null auto_increment, `fecha_inicial` date not null,`fecha_final` date not null, `valor` double not null, `fecha` date not null, `observacion` char(250) not null,`usuario` char(20) not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;

create table if not exists `tools`.`cuotas_efectivo`(`id` int(9) not null auto_increment,`id_efectivo` int(57) not null, `observacion` char(250) not null, `valor` double not null,`fecha` date not null, PRIMARY KEY (`id`)) ENGINE=InnoDB;


alter table `tools`.`ingreso_saldos` change if exists `placa` `cedula` char(32) not null;

ALTER TABLE `tools`.`devoluciones` ADD if not exists `valor` double not null after `usuario`;


ALTER TABLE `tools`.`saldos` ADD if not exists `identificacion` char(20) not null after `fecha_limite`;
ALTER TABLE `tools`.`ingreso_saldos` ADD if not exists `identificacion` char(20) not null after `fecha_limite`;
ALTER TABLE `tools`.`ingreso_saldos` DROP if exists `cedula`;

alter table `tools`.`devoluciones` change if exists `usuario` `usuario` char(30) not null;
alter table `tools`.`devoluciones` change if exists `valor` `valor` double not null;

create unique index if not exists proveedores_nombre on `tools`.`proveedores`(proveedor);

ALTER TABLE `tools`.`clientes` ADD if not exists `tipo` int(3) not null after `debe`;

